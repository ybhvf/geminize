import { DefaultProxies } from './proxies.js';

function getRandomInt(max) {
      return Math.floor(Math.random() * Math.floor(max));
}

export const DefaultOptions = {
    customProxy: '',
    proxyChoice: ['Mozz.us', 'Volpes'][getRandomInt(2)]
}

export function getOptions() {
    return browser.storage.sync.get(DefaultOptions);
}

export function getProxyList() {
    function onError(error) {
        console.log(`Error: ${error}`);
    }

    return browser.storage.sync.get(DefaultOptions).then(
        result => {
            // is this really how it's done in javascript?
            let proxies = JSON.parse(JSON.stringify(DefaultProxies));

            if (result.customProxy != '') {
                proxies.push({ name: 'Custom', url: result.customProxy });
            }

            for (let proxy of proxies) {
                proxy.default = false;
                if (proxy.name == result.proxyChoice) {
                    proxy.default = true;
                }
            }

            return proxies;
        },
        onError
    );
}

