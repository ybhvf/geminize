export const DefaultProxies = [
    { name: 'Mozz.us', url: 'https://portal.mozz.us/gemini/' },
    { name: 'Volpes', url: 'https://proxy.vulpes.one/gemini/' },
]
